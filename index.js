// Lesson 3
function fizzBuzz (num) {
    for (let i = 1; i <= num; i++) {
        if(i % 3 == 0 && i % 5 == 0){
            console.log("FizzBuzz")
        }else if(i % 5 == 0){
            console.log("Buzz")
        }else if(i % 3 == 0 ){
            console.log("Fizz")
        }else{
            console.log(i)
        }
    }
}

fizzBuzz(50);

function filterArray (list) {
    const newList = [];
    for (const i of list) {
        if(Array.isArray(i)){
            newList.push(...i)
        }else{
            newList.push(i)
        }
    }

    if(typeof(newList[0] == "numbers")){
        const res = [];
        for (const i in newList) {
             if(typeof(newList[i]) == "number"){
                res.push(newList[i])
            }
        }
        return res;
    }    
}

const letList = [
    [2], 
    23,
    'dance',
    [7,8,9, 7],
    true, 
    [3, 5, 3],
    [777,855, 9677, 457],
]

const newLetList = filterArray(letList);
const result = newLetList.filter((value, index, self) => self.indexOf(value) === index); 

console.log(result)